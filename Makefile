BIN = bin

default: openmp

mpi:
	mpicc -lm main.c table.h -o $(BIN)/gomory
	
openmp:
	mpicc -lm main.c table.h -o $(BIN)/gomory -fopenmp
	
submit:
	cd $(BIN) && llsubmit small.job
