#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>

#include "table.h"

#define EPS 0.000001

#define ROW_WIDTH (1 + N + M)
#define MAX_ROW_WIDTH (1 + N + M * 2)

#ifdef _DEBUG
	#define DEBUG_PRINTF printf
#else
	#define DEBUG_PRINTF
#endif

#ifdef _DEBUG
void print_result(int rank, double **C, int c_width, int c_height)
{
	int i, j;

	printf("#%d: table\n", rank);
	for (i = 0; i < c_height; ++i)
	{
		for (j = 0; j < c_width; ++j)
		{
			printf("%.2lf\t", C[i][j]);
		}
		printf("\n");
	}
}

void print_func(int rank, double *f, int c_width)
{
	int i;
	printf("#%d: function\n", rank);
	for (i = 0; i < c_width; ++i)
		printf("%.2lf\t", f[i]);
	printf("\n");
}
#endif

int is_integer(double value)
{
	double v = value - floor(value);
	return (v < EPS || v > 1 - EPS) ? 1 : 0;
}

int main(int argc, char** argv)
{
	MPI_Status status;
	int provided;
	int rank, procs;
	int i, j, k;
	double time_start, time_finish;

	double **C;           // локальная таблица процесса
	int c_height;         // количество строк в C
	int c_width;
	double q[MAX_ROW_WIDTH];  // строка таблицы с коэффициентами функции
	double f[MAX_ROW_WIDTH];  // строка таблицы с коэффициентами текущей целевой функции
	double g[MAX_ROW_WIDTH];  // строка таблицы для обмена между процессами
	int *basis;           // номера базисных переменных для строки

	int column = -1;
	int gomory_iter = -1;
	int iter = 0;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0)
	{
		switch (provided)
		{
		case MPI_THREAD_SINGLE: printf("Provided MPI flag: MPI_THREAD_SINGLE\n"); break;
		case MPI_THREAD_FUNNELED: printf("Provided MPI flag: MPI_THREAD_FUNNELED\n"); break;
		case MPI_THREAD_SERIALIZED: printf("Provided MPI flag: MPI_THREAD_SERIALIZED\n"); break;
		case MPI_THREAD_MULTIPLE: printf("Provided MPI flag: MPI_THREAD_MULTIPLE\n"); break;
		default: printf("Provided MPI flag: unknown\n"); break;
		}
	}

	time_start = MPI_Wtime();
	printf("#%d: started at %f\n", rank, time_start);

#ifdef _OPENMP
	printf("#%d: running with OpenMP!\n", rank);
#endif
		
	// === ШАГ 1 ===
	// Каждый процесс берет себе несколько строк таблицы.

	c_height = M / procs;
	c_width  = ROW_WIDTH;
	
	C = (double**) malloc(c_height * 2 * sizeof(double*));
	basis = (int*) malloc(c_height * 2 * sizeof(int));
	
	for (i = 0; i < c_height * 2; ++i)
	{
		C[i] = (double*) malloc(MAX_ROW_WIDTH * sizeof(double));
	}
	
	for (i = 0; i < c_height; ++i)
	{
		for (j = 0; j < ROW_WIDTH; ++j)
			C[i][j] = table[rank * c_height + i][j];
		
		basis[i] = N + rank * c_height + i;
	}
	
	if (rank == 0)
	{
		for (j = 0; j < ROW_WIDTH; ++j)
		{
			f[j] = q[j] = table[M][j];
		}
	}
	
gomory_start:
	
	if (gomory_iter >= 0)
	{		
		for (i = 0; i < c_height; ++i)
			C[i][c_width] = 0;
		
		int gomory_rank = gomory_iter % procs;
		if (rank == gomory_rank)
		{
			int gomory_row = gomory_iter / procs;
			
			DEBUG_PRINTF("#%d gomory_iter = %d, gomory_row = %d\n", rank, gomory_iter, gomory_row);
				
			for (j = 0; j < c_width; ++j)
			{
				g[j] = C[c_height][j] = -(C[gomory_row][j] - floor(C[gomory_row][j]));
			}
			
			C[c_height][c_width] = 1;
			basis[c_height] = c_width;
			++c_height;
			
			if (gomory_rank != 0)
			{
				MPI_Send(&g, c_width, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
			}
		}
		
		if (rank == 0)
		{
			if (gomory_rank != 0)
			{
				MPI_Recv(&g, c_width, MPI_DOUBLE, gomory_rank, 1, MPI_COMM_WORLD, &status);
			}
			
			for (j = 0; j < c_width; ++j)
			{
				f[j] = g[j];
			}
			q[c_width] = f[c_width] = 0;
		}
		++c_width;
	}
	
	// === ШАГ 2 ===
	// Главный процесс находит столбец с отрицательным коэффициентом в векторе
	// коэффициентов функции и рассылает его номер остальным процессам. Если
	// такой столбец отсутствует, решение найдено. Найденный столбец называется
	// разрешающим.

step_2:
	iter++;

#ifdef _DEBUG
	if (M <= 10)
	{
		print_result(rank, C, c_width, c_height);
		if (rank == 0) print_func(rank, f, c_width);
	}
#endif

	column = -1;
	if (rank == 0)
	{
		double xmin = DBL_MAX;
		DEBUG_PRINTF("#%d iter = %d\n", rank, iter);

		#pragma omp parallel private(i) shared(xmin, column)
		{
			double my_xmin = DBL_MAX;
			int my_column = -1;

#ifdef _OPENMP
			DEBUG_PRINTF("#%d (%d): thread %d\n", rank, iter, omp_get_thread_num());
#endif

			#pragma omp for nowait
			for (i = 1; i <= c_width; ++i)
			{
				if (f[i] < -EPS && f[i] < my_xmin) // ~ f[i] < 0
				{
					my_xmin = f[i];
					my_column = i;
				}
			}
			#pragma omp critical
			{
				if (my_xmin < xmin)
				{
					xmin = my_xmin;
					column = my_column;
				}
			}
		}

		DEBUG_PRINTF("#%d (%d): pivot column = %d; xmin = %.2lf\n", rank, iter, column,
			xmin < DBL_MAX-1 ? xmin : -1);
		
		for (k = 1; k < procs; ++k)
			MPI_Send(&column, 1, MPI_INT, k, 1, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Recv(&column, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
	}

	if (column < 0)
	{
		if (rank == 0)
			DEBUG_PRINTF("\n\nSOLUTION FOUND!\n");
		goto solution_found;
	}

	// === ШАГ 3 ===
	// Каждый процесс ищет строку, в которой коэффициент в разрешающем столбце
	// положителен и отношение соответствующего значения вектора свободных
	// членов к значению этого коэффициента будет минимальным. Если строка найдена,
	// процесс запоминает это отношение и номер строки.

	double xmin = DBL_MAX;
	int row_no = -1;

	#pragma omp parallel private(j) shared(xmin, row_no)
	{
		double my_xmin = DBL_MAX;
		int my_row_no = -1;

		#pragma omp for nowait
		for (j = 0; j < c_height; ++j)
			if (C[j][column] > EPS) // ~ C[j][column] > 0
			{
				double x = C[j][0] / C[j][column];
				if (x < my_xmin)
				{
					my_xmin = x;
					my_row_no = j;
				}
			}
		#pragma omp critical
		{
			if (my_xmin < xmin)
			{
				xmin = my_xmin;
				row_no = my_row_no;
			}
		}
	}

	DEBUG_PRINTF("#%d (%d): pivot row = %d; min(b/a) = %.4lf\n", rank, iter, row_no,
	   xmin < DBL_MAX-1 ? xmin : -1);

	// === ШАГ 4 ===
	// Каждый процесс отсылает найденное минимальное значение главному процессу.
	// Тот, в свою очередь, находит минимальное из всех присланных, и рассылает
	// всем номер процесса, который его прислал. Если же минимальное значение не
	// найдено (нету строк с положительным коэффициентом), задача не имеет решения.

	int min_L;
	if (rank != 0)
	{
		MPI_Send(&row_no, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		MPI_Send(&xmin, 1, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
		MPI_Recv(&min_L, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
	}
	else
	{
		min_L = (row_no >= 0) ? 0 : -1;
		for (k = 1; k < procs; ++k)
		{
			double x;
			int r;
			MPI_Recv(&r, 1, MPI_INT, k, 1, MPI_COMM_WORLD, &status);
			MPI_Recv(&x, 1, MPI_DOUBLE, k, 1, MPI_COMM_WORLD, &status);
			if (r >= 0 && x < xmin)
			{
				xmin = x;
				row_no = r;
				min_L = k;
			}
		}

		for (k = 1; k < procs; ++k)
		{
			MPI_Send(&min_L, 1, MPI_INT, k, 1, MPI_COMM_WORLD);
		}
	}

	if (min_L == -1)
	{
		if (rank == 0) printf("\n\nNO SOLUTION!\n");
		goto out;
	}

	DEBUG_PRINTF("#%d (%d): pivot process = %d\n", rank, iter, min_L);

	// === ШАГ 5 ===
	// "Процесс-победитель" отсылает всем остальным процессам строку с запомненным
	// ранее номером и соответствующее значение вектора свободных членов. Найденная
	// строка называется разрешающей.

	if (rank == min_L)
	{
		for (j = 0; j < c_width; ++j)
			g[j] = C[row_no][j];
		basis[row_no] = column;
		
		for (k = 0; k < procs; ++k)
		{
			if (k != rank)
			{
				MPI_Send(&g, c_width, MPI_DOUBLE, k, 1, MPI_COMM_WORLD);
			}
		}
	}
	else
	{
		MPI_Recv(&g, c_width, MPI_DOUBLE, min_L, 1, MPI_COMM_WORLD, &status);
	}

	DEBUG_PRINTF("#%d (%d): pivot row free quot = %lf\n", rank, iter, g[0]);

	// === ШАГ 6 ===
	// Каждый процесс проводит симплекс-преобразование для своей части таблицы.
	// Из каждого элемента таблицы вычитается отношение C[i][k] / g[k] * g[j],
	// где i,j - индексы строки и столбца, g - разрешающая строка, k - индекс
	// разрешающего столбца. Аналогичные вычисления проводятся с частью вектора
	// свободных членов. Элемент g[k] (находящийся на пересечении разрешающего
	// столбца и разрешающей строки) называется разрешающим элементом.

	DEBUG_PRINTF("#%d (%d): simplex transformation (step 6)\n", rank, iter);

	#pragma omp parallel for private(i, j) shared(C)
	for (i = 0; i < c_height; ++i)
	{
		double a = C[i][column] / g[column];
		for (j = 0; j < c_width; ++j)
		{
			C[i][j] = C[i][j] - a * g[j];
		}
	}

	// === ШАГ 7 ===
	// "Процесс-победитель" делит разрешающую строку на разрешающий элемент (тоже
	// часть симплекс-преобразования).

	DEBUG_PRINTF("#%d (%d): simplex transformation (step 7)\n", rank, iter);
	if (rank == min_L)
	{
		#pragma omp parallel for private(j) shared(C)
		for (j = 0; j < c_width; ++j)
		{
			C[row_no][j] = g[j] / g[column];
		}
	}

	// === ШАГ 8 ===
	// Главный процесс осуществляет симплекс-преобразование с вектором коэффициентов
	// функции.

	DEBUG_PRINTF("#%d (%d): simplex transformation (step 8)\n", rank, iter);
	if (rank == 0)
	{
		double aq = q[column] / g[column];
		double af = f[column] / g[column];

		#pragma omp parallel for private(j) shared(q, f)
		for (j = 0; j < c_width; ++j)
		{
			q[j] = q[j] - aq * g[j];
			f[j] = f[j] - af * g[j];
		}
	}

	// Симплекс-преобразование окончено, переходим снова ко второму шагу.
	goto step_2;
    
solution_found:
	DEBUG_PRINTF("\n");
	for (i = 0; i < c_height; ++i)
	{
		DEBUG_PRINTF("#%d (%d): X%03d = %lf\n", rank, iter, basis[i], C[i][0]);
	}
	
	if(rank == 0)
	{
		DEBUG_PRINTF("Solution is %lf\n", q[0]);
	}
	
	++gomory_iter;

	if (gomory_iter >= M)
	{
		printf("Max iters reached. Problem not solved :(\n\n\n");
		goto out;
	}

	for (i = 0; i < c_height; ++i)
		if (!is_integer(C[i][0]))
		{
			DEBUG_PRINTF("Not all variables are integer. Next Gomory iteration.\n\n\n");
			goto gomory_start;
		}

	DEBUG_PRINTF("\n\n\n");
	printf("All variables are integer. Problem solved!\n");
	
	for (i = 0; i < c_height; ++i)
	{
		printf("#%d (%d): X%03d = %lf\n", rank, iter, basis[i], C[i][0]);
	}
	
	if(rank == 0)
	{
		printf("Solution is %lf\n", q[0]);
	}

out:
	time_finish = MPI_Wtime();
	printf("#%d: finished at %f, elapsed %f\n", rank, time_finish, time_finish - time_start);
	MPI_Finalize();
	return 0;
}
