import sys, random

a_min = -700
a_max = 1800

b_min = 1
b_max = 500

c_min = -500
c_max = -1

size = int(sys.argv[1])
rows = size + 1
cols = size*2 + 1

print '''
#ifndef INCLUDE_ONCE_PY_H
#define INCLUDE_ONCE_PY_H
	
#define M %d
#define N %d
	
typedef double		elem_t;
typedef elem_t*		vector_t;
typedef vector_t*	matrix_t;
	
double table[M + 1][M + N + 1] = ''' % (size, size)

matr = []
for i in range(size):
	vec = []
	vec.append(random.randrange(b_min, b_max))
	for j in range(size): vec.append(random.randrange(a_min, a_max))
	for j in range(size, size*2):
		if j - size == i:
			vec.append(1)
		else:
			vec.append(0)
	matr.append(vec)

vec = []
for j in range(size): vec.append(random.randrange(c_min, c_max))
for j in range(size, size*2+1): vec.append(0)
matr.append(vec)

lines = []
for vec in matr:
	lines.append('    { ' + ',\t\t'.join(str(x) for x in vec) + ' }')
print '{\n' + ',\n'.join(lines) + '\n};'
		
print '\n\n#endif'
